import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { AuthService } from '../services/auth.service';

export const roleGuard: CanActivateFn = (route, state) => {
  const authService = inject(AuthService);
  const router = inject(Router)

  authService.loadCurrentUser()
  const userRole = authService.getRole();
  const requiredRole = route.data?.['role'];

  if (userRole && requiredRole === userRole) {
    return true;
  }
  // Redirect if the user does not have the required role
  return router.createUrlTree(['/unauthorized']);
};
