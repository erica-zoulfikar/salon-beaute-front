import { Component, TemplateRef, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Subscription, interval, switchMap } from 'rxjs';
import { EmployeeService } from '../../../services/employee.service';
import { TableComponent } from '../../../components/table/table.component';
import { AlertComponent } from '../../../alert/alert.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-settings',
  standalone: true,
  imports: [TableComponent, AlertComponent, CommonModule, ReactiveFormsModule],
  templateUrl: './employee-settings.component.html',
})
export class EmployeeSettingsComponent {
  @ViewChild('actionTemplate') actionTemplate!: TemplateRef<any>;

  columns = [
    { key: 'username', title: 'NOM D\'UTILISATEUR' },
    { key: 'email', title: 'EMAIL' },
    { key: 'job', title: 'JOB' },
    { key: 'gender', title: 'GENRE' },
    { key: 'ACTIONS', title: 'ACTIONS', template: this.actionTemplate },
  ];

  data: any[] = [];
  createForm: FormGroup;
  editForm: FormGroup;
  deleteForm: FormGroup;
  pollingSubscription!: Subscription;
  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';

  constructor(
    private fb: FormBuilder, 
    private employeeService: EmployeeService,
    private ngZone: NgZone
  ) {
    this.createForm = this.fb.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      role: ['employee', [Validators.required]],
      gender: ['', [Validators.required]],
      job: ['', [Validators.required]],
      employment_date: ['', [Validators.required]],
    });
    this.editForm = this.fb.group({
      userId: ['', [Validators.required]],
      username: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      job: ['', [Validators.required]],
      employment_date: ['', [Validators.required]],
    });
    this.deleteForm = this.fb.group({
      userId: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.fetchData();
    this.ngZone.runOutsideAngular(() => {
      this.pollingSubscription = interval(10000)
        .pipe(
          switchMap(() => this.employeeService.getEmployees()),
        ).subscribe((res) => {
          this.ngZone.run(() => {
            this.data = res
          });
        });
    });
  }

  ngOnDestroy() {
    if (this.pollingSubscription) {
      this.pollingSubscription.unsubscribe();
    }
  }

  fetchData() {
    this.employeeService.getEmployees().subscribe({
      next: (res) => {
        this.data = res
      },
      error: (err) => {
        this.alertMessage = err.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
  }

  openModalEdit(userId: string): void {
    this.employeeService.getEmployeeById(userId).subscribe(user => {
      this.editForm.setValue({
        userId: user._id,
        gender: user.gender,
        job: user.job,
        employment_date: user.employment_date
      });
    });
  }

  openModalDelete(userId: string): void {
    this.deleteForm.setValue({
      userId: userId
    });
    this.fetchData()
  };

  onSubmitCreate() {
    this.employeeService.createEmployee(
      this.createForm.value.email,
      this.createForm.value.username,
      this.createForm.value.password,
      this.createForm.value.role,
      this.createForm.value.gender,
      this.createForm.value.job,
      this.createForm.value.employment_date
    ).subscribe({
      next: res => {
        this.alertMessage = res.message;
        this.alertType = 'success';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
    this.fetchData();
  }

  onSubmitEdit() {
    this.employeeService.updateEmployee(
      this.editForm.value.userId,
      this.editForm.value.username,
      this.editForm.value.gender,
      this.editForm.value.job,
      this.editForm.value.employment_date
    ).subscribe({
      next: res => {
        this.alertMessage = res.message;
        this.alertType = 'success';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
    this.fetchData();
  }

  onSubmitDelete() {
    this.employeeService.deleteEmployee(this.deleteForm.value.userId).subscribe({
      next: res => {
        this.alertMessage = res.message;
        this.alertType = 'success';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
    this.fetchData();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const actionsColumn = this.columns.find(column => column.key === 'ACTIONS');
      if (actionsColumn && this.actionTemplate) {
        actionsColumn.template = this.actionTemplate;
      }
    });
  }
}
