import { Component, TemplateRef, ViewChild, OnInit, OnDestroy, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Subscription, interval, switchMap } from 'rxjs';
import { ServiceService } from '../../../services/service.service';
import { TableComponent } from '../../../components/table/table.component';
import { AlertComponent } from '../../../alert/alert.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-settings',
  standalone: true,
  imports: [TableComponent, AlertComponent, CommonModule, ReactiveFormsModule],
  templateUrl: './service-settings.component.html',
})
export class ServiceSettingsComponent {
  @ViewChild('priceTemplate') priceTemplate!: TemplateRef<any>;
  @ViewChild('durationTemplate') durationTemplate!: TemplateRef<any>;
  @ViewChild('commissionTemplate') commissionTemplate!: TemplateRef<any>;
  @ViewChild('actionTemplate') actionTemplate!: TemplateRef<any>;

  columns = [
    { key: 'label', title: 'LABEL' },
    { key: 'duration', title: 'PRICE', template: this.durationTemplate },
    { key: 'price', title: 'DURATION', template: this.priceTemplate },
    { key: 'commission', title: 'COMMISSION', template: this.commissionTemplate },
    { key: 'description', title: 'DESCRIPTION' },
    { key: 'ACTIONS', title: 'ACTIONS', template: this.actionTemplate },
  ];

  data: any[] = [];
  createForm: FormGroup;
  editForm: FormGroup;
  deleteForm: FormGroup;
  pollingSubscription!: Subscription;
  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';

  formatPrice(price: string) {
    const number = Number(price);
    const formattedNumber = new Intl.NumberFormat('en-US').format(number);
    return `${formattedNumber} Ar`;
  }

  constructor(
    private fb: FormBuilder, 
    private serviceService: ServiceService,
    private ngZone: NgZone
  ) {
    this.createForm = this.fb.group({
      label: ['', [Validators.required, Validators.email]],
      duration: ['', [Validators.required]],
      price: ['service', [Validators.required]],
      commission: ['', [Validators.required]],
      description: ['']
    });
    this.editForm = this.fb.group({
      id: ['', [Validators.required]],
      label: ['', [Validators.required, Validators.email]],
      duration: ['', [Validators.required]],
      price: ['service', [Validators.required]],
      commission: ['', [Validators.required]],
      description: ['']
    });
    this.deleteForm = this.fb.group({
      id: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.fetchData();
    this.ngZone.runOutsideAngular(() => {
      this.pollingSubscription = interval(10000)
        .pipe(
          switchMap(() => this.serviceService.getServices()),
        ).subscribe((res) => {
          this.ngZone.run(() => {
            this.data = res
          });
        });
    });
  }

  ngOnDestroy() {
    if (this.pollingSubscription) {
      this.pollingSubscription.unsubscribe();
    }
  }

  fetchData() {
    this.serviceService.getServices().subscribe({
      next: (res) => {
        this.data = res
      },
      error: (err) => {
        this.alertMessage = err.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
  }

  openModalEdit(id: string): void {
    this.serviceService.getServiceById(id).subscribe(service => {
      this.editForm.setValue({
        id: service._id,
        label: service.label,
        duration: service.duration,
        price: service.price,
        commission: service.commission,
        description: service.description,
        image: service.image
      });
    });
  }

  openModalDelete(id: string): void {
    this.deleteForm.setValue({
      id: id
    });
    this.fetchData()
  };

  onSubmitCreate() {
    this.serviceService.createService(
      this.createForm.value.label,
      this.createForm.value.duration,
      this.createForm.value.price,
      this.createForm.value.commission,
      this.createForm.value.description,
      this.createForm.value.image
    ).subscribe({
      next: res => {
        this.alertMessage = res.message || 'Succès';
        this.alertType = 'success';
        this.showAlert = true;
        this.fetchData();
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
  }

  onSubmitEdit() {
    this.serviceService.updateService(
      this.editForm.value.id,
      this.editForm.value.label,
      this.editForm.value.duration,
      this.editForm.value.price,
      this.editForm.value.commission,
      this.editForm.value.description,
      this.editForm.value.image
    ).subscribe({
      next: res => {
        this.alertMessage = res.message || 'Succès';
        this.alertType = 'success';
        this.showAlert = true;    
        this.fetchData();
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
  }

  onSubmitDelete() {
    this.serviceService.deleteService(this.deleteForm.value.id).subscribe({
      next: res => {
        this.alertMessage = res.message;
        this.alertType = 'success';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
    this.fetchData();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const durationColumn = this.columns.find(column => column.title === 'DURATION');
      if (durationColumn && this.durationTemplate) {
        durationColumn.template = this.durationTemplate;
      }
      const priceColumn = this.columns.find(column => column.title === 'PRICE');
      if (priceColumn && this.priceTemplate) {
        priceColumn.template = this.priceTemplate;
      }
      const commissionColumn = this.columns.find(column => column.title === 'COMMISSION');
      if (commissionColumn && this.commissionTemplate) {
        commissionColumn.template = this.commissionTemplate;
      }
      const actionsColumn = this.columns.find(column => column.key === 'ACTIONS');
      if (actionsColumn && this.actionTemplate) {
        actionsColumn.template = this.actionTemplate;
      }
    });
  }
}
