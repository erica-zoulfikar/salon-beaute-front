import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageWorkHourStatComponent } from './average-work-hour-stat.component';

describe('AverageWorkHourStatComponent', () => {
  let component: AverageWorkHourStatComponent;
  let fixture: ComponentFixture<AverageWorkHourStatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AverageWorkHourStatComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AverageWorkHourStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
