import { Component, TemplateRef, ViewChild } from '@angular/core';
import { StatsService } from '../../../services/stats.service';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from '../../../components/table/table.component';

@Component({
  selector: 'app-average-work-hour-stat',
  standalone: true,
  imports: [ReactiveFormsModule, TableComponent],
  templateUrl: './average-work-hour-stat.component.html',
  styleUrl: './average-work-hour-stat.component.scss'
})
export class AverageWorkHourStatComponent {
  @ViewChild('avg') avgTemplate!: TemplateRef<any>;

  columns = [
    { key: 'label', title: 'EMPLOYE' },
    { key: 'data', title: 'MOYENNE', template: this.avgTemplate },
  ];

  data!: any[]
  
  averageWorkHourForm!:any

  formatData(data: any) {
    return `${data} H`
  }

  constructor(private statsService:StatsService, private fb:FormBuilder){
    const currentDate = new Date()
    this.averageWorkHourForm = this.fb.group({
      type: ['jour'],
      date: [currentDate]
    })

    this.statsService.getAverageWorkTimeEmp(currentDate, 'jour').subscribe({
      next: res => {
        console.log(res)
        this.data = res
      }
    })
  }

  onSubmitAverageWorkHour() {
    const startDate = new Date(this.averageWorkHourForm.value.date);
    
      this.statsService.getChiffreDaffaireJour(
        startDate, 
        new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0)).subscribe({
        next: res => {
          this.data = res.data
        }
      })
  }
  
  ngAfterViewInit() {
    setTimeout(() => {
      const avgColumn = this.columns.find(column => column.key === 'MOYENNE');
      if (avgColumn && this.avgTemplate) {
        avgColumn.template = this.avgTemplate;
      }
    });
  }

}
