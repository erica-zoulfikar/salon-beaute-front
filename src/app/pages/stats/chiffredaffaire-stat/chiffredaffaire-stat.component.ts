import { Component, } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BarChartComponent } from '../../../components/bar-chart/bar-chart.component';
import { LineChartComponent } from '../../../components/line-chart/line-chart.component';
import { StatsService } from '../../../services/stats.service';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [BarChartComponent, LineChartComponent, ReactiveFormsModule, CommonModule],
  templateUrl: './chiffredaffaire-stat.component.html'
})
export class ChiffredaffaireStatComponent {
  tableData!: any;

  lineChartLabels!: any;
  lineChartData!: any;
  lineChartTitle!: any;
  
  chiffreDaffaireForm!:any

  constructor(private statsService: StatsService, private fb: FormBuilder) {
    const currentDate = new Date()
    this.chiffreDaffaireForm = this.fb.group({
      type: ['jour'],
      dateDebut: [currentDate]
    })

    this.statsService.getChiffreDaffaireJour(
      new Date(currentDate.getFullYear(), currentDate.getMonth(), 1), 
      new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0)).subscribe({
      next: res => {
        this.lineChartData = res.data
        this.lineChartLabels = res.labels
        this.lineChartTitle = res.title
      }
    })
  };

  onSubmitChiffreDaffaire() {
    const startDate = new Date(this.chiffreDaffaireForm.value.dateDebut);
    
    if(this.chiffreDaffaireForm.value.type == 'day'){
      this.statsService.getChiffreDaffaireJour(
        startDate, 
        new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0)).subscribe({
        next: res => {
          this.lineChartData = res.data
          this.lineChartLabels = res.labels
          this.lineChartTitle = res.title
        }
      })
    } else {
      this.statsService.getChiffreDaffaireMois(
        new Date(startDate.getFullYear(), 0, 1), 
        new Date(startDate.getFullYear() + 1, 0, 0)).subscribe({
        next: res => {
          this.lineChartData = res.data
          this.lineChartLabels = res.labels
          this.lineChartTitle = res.title
        }
      })
    }
  }

  ngAfterViewInit() {
  }
}
