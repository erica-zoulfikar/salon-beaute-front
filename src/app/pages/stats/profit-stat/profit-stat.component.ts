import { Component, } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BarChartComponent } from '../../../components/bar-chart/bar-chart.component';
import { LineChartComponent } from '../../../components/line-chart/line-chart.component';
import { StatsService } from '../../../services/stats.service';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [BarChartComponent, LineChartComponent, ReactiveFormsModule, CommonModule],
  templateUrl: './profit-stat.component.html'
})
export class ProfitStatComponent {
  tableData!: any;

  lineChartLabels!: any;
  lineChartData!: any;
  lineChartTitle!: any;
  
  profitForm!:any

  constructor(private statsService: StatsService, private fb: FormBuilder) {
    const currentDate = new Date()
    this.profitForm = this.fb.group({
      type: ['day'],
      dateDebut: [currentDate],
      salaire: ['0'],
      loyer: ['0'],
      achatPiece: ['0'],
      autresDepenses: ['0']
    })

    this.statsService.getProfitPerMonth(
      new Date(currentDate.getFullYear(),0, 1), 
      new Date(currentDate.getFullYear(), 11, 0),
      this.profitForm.value.salaire,
      this.profitForm.value.loyer,
      this.profitForm.value.achatPiece,
      this.profitForm.value.autresDepenses
      ).subscribe({
      next: res => {
        this.lineChartData = res.data
        this.lineChartLabels = res.labels
        this.lineChartTitle = res.title
      }
    })
  };

  onSubmitProfit() {
    const startDate = new Date(this.profitForm.value.dateDebut);
    
    this.statsService.getProfitPerMonth(
      new Date(startDate.getFullYear(), 0, 1),
      new Date(startDate.getFullYear(), 11, 0),
      this.profitForm.value.salaire,
      this.profitForm.value.loyer,
      this.profitForm.value.achatPiece,
      this.profitForm.value.autresDepenses
    ).subscribe({
      next: res => {
        this.lineChartData = res.data
        this.lineChartLabels = res.labels
        this.lineChartTitle = res.title
      }
    })
  }

  ngAfterViewInit() {
  }
}
