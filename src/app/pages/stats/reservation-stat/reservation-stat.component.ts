import { Component } from '@angular/core';
import { StatsService } from '../../../services/stats.service';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BarChartComponent } from '../../../components/bar-chart/bar-chart.component';
import { LineChartComponent } from '../../../components/line-chart/line-chart.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-reservation-stat',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, BarChartComponent, LineChartComponent],
  templateUrl: './reservation-stat.component.html',
  styleUrl: './reservation-stat.component.scss'
})
export class ReservationStatComponent {
  tableData!: any;
  barChartLabels!: any;
  barChartData!: any;
  barChartTitle!: any;
  
  reservationPerDayForm!:any

  tableColumn = [
    { key: 'email', title: 'EMAIL' },
    { key: 'job', title: 'JOB' },
    { key: 'gender', title: 'GENRE' },
    { key: 'employment_date', title: 'DATE D\'EMPLOIEMENT' },
  ];

  constructor(private statsService: StatsService, private fb: FormBuilder) {
    const currentDate = new Date()
    this.reservationPerDayForm = this.fb.group({
      type: ['day'],
      startDate: [currentDate],
    })

    this.statsService.getReservationsPerDay(new Date(currentDate.getFullYear(), 1, 1), new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0)).subscribe({
      next: res => {
        this.barChartData = res.data
        this.barChartLabels = res.labels
        this.barChartTitle = res.title
      }
    })
  };

  onSubmitReservation() {
    const startDate = new Date(this.reservationPerDayForm.value.startDate);
    
    if(this.reservationPerDayForm.value.type === 'day'){
      this.statsService.getReservationsPerDay(startDate, new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0)).subscribe({
        next: res => {
          this.barChartData = res.data
          this.barChartLabels = res.labels
          this.barChartTitle = res.title
        }
      })
    } else {
      this.statsService.getReservationsPerMonth(new Date(startDate.getFullYear(), 0, 1), new Date(startDate.getFullYear() + 1, 0, 0)).subscribe({
        next: res => {
          this.barChartData = res.data
          this.barChartLabels = res.labels
          this.barChartTitle = res.title
        }
      })
    }
  }

  ngAfterViewInit() {
  }
}
