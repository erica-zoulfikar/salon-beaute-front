import { Component, TemplateRef, ViewChild } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { BarChartComponent } from '../../components/bar-chart/bar-chart.component';
import { StatsService } from '../../services/stats.service';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [TableComponent, BarChartComponent, TableComponent, ReactiveFormsModule, CommonModule],
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
  tableData!: any;
  barChartLabels!: any;
  barChartData!: any;
  barChartTitle!: any;
  
  reservationPerDayForm!:any

  tableColumn = [
    { key: 'email', title: 'EMAIL' },
    { key: 'job', title: 'JOB' },
    { key: 'gender', title: 'GENRE' },
    { key: 'employment_date', title: 'DATE D\'EMPLOIEMENT' },
  ];

  constructor(private statsService: StatsService, private fb: FormBuilder) {
    const currentDate = new Date()
    this.reservationPerDayForm = this.fb.group({
      type: ['day'],
      startDate: [new Date(currentDate.getFullYear(),currentDate.getMonth(), 0)],
    })

    this.statsService.getReservationsPerDay(new Date(currentDate.getFullYear(), 1, 1), new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0)).subscribe({
      next: res => {
        this.barChartData = res.data
        this.barChartLabels = res.labels
        this.barChartTitle = res.title
      }
    })
  };

  onSubmitReservation() {
    const startDate = new Date(this.reservationPerDayForm.value.startDate);
    
    if(this.reservationPerDayForm.value.type === 'day'){
      this.statsService.getReservationsPerDay(startDate, new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0)).subscribe({
        next: res => {
          this.barChartData = res.data
          this.barChartLabels = res.labels
          this.barChartTitle = res.title
        }
      })
    } else {
      this.statsService.getReservationsPerMonth(new Date(startDate.getFullYear(), 0, 1), new Date(startDate.getFullYear() + 1, 0, 0)).subscribe({
        next: res => {
          this.barChartData = res.data
          this.barChartLabels = res.labels
          this.barChartTitle = res.title
        }
      })
    }
  }

  ngAfterViewInit() {
  }
}
