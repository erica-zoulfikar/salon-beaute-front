import { Component } from '@angular/core';
import { CalendarModule } from '../../components/calendar/calendar.module';
import { AuthService } from '../../services/auth.service';
import { AppointmentsService } from '../../services/appointments.service';
import { AppointmentCardComponent } from '../../components/appointment-card/appointment-card.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-appointments',
  standalone: true,
  imports: [CommonModule, CalendarModule, AppointmentCardComponent],
  templateUrl: './appointments.component.html',
  styleUrl: './appointments.component.scss'
})
export class AppointmentsComponent {
  currentUser: any = {};
  
  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';

  pending!: any[]

  constructor(
    private auth: AuthService,
    private appointmentService: AppointmentsService,
  ) {
    this.auth.loadCurrentUser();
    this.currentUser = this.auth.getCurrentUser();
    this.loadAppointments()
  }

  loadAppointments() {
    this.appointmentService.getAppointmentsPendingWoDate().subscribe({
      next: res => {
        this.pending = res
      }
    })
  }

  confirmAppointment(appointment_id: String) {
    this.appointmentService.confirmAppointment(appointment_id).subscribe({
      next: res => {
        this.loadAppointments()
      }
    })
  }

  cancelAppointment(appointment_id: String) {
    this.appointmentService.cancelAppointment(appointment_id).subscribe({
      next: res => {
        this.loadAppointments()
      }
    })
  }

  loadEvents(from: any, to: any) {
    return this.appointmentService.getAppointmentsPending(from, to);
  }

  createAppointment(id_service: String, id_employee: String, date: String, start: String, end: String, text: String) {
    return this.appointmentService.createAppointment(id_service, id_employee, date, start, end, text);
  }

  updateAppointment(id_appointment: String, id_service: String, id_employee: String, date: String, start: String, end: String, text: String) {
    return this.appointmentService.updateAppointment(id_appointment, id_service, id_employee, date, start, end, text);
  }

  deleteAppointment(id_appointment: String) {
    return this.appointmentService.deleteAppointment(id_appointment);
  }
}
