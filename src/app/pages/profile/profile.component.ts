import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, RouterLink } from '@angular/router';
import { CommonModule, Location } from '@angular/common';
import { SpinnerComponent } from '../../components/spinner/spinner.component';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { EmployeeService } from '../../services/employee.service';
import { ServiceService } from '../../services/service.service';
import { ClientServiceService } from '../../services/client.service';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [CommonModule, RouterLink, SpinnerComponent, ReactiveFormsModule, AlertComponent],
  templateUrl: './profile.component.html',
  providers: [AuthService]
})
export class ProfileComponent {
  currentUser: any = {};

  updateClientForm!: FormGroup;
  updateEmployeeForm!: FormGroup;
  updateManagerForm!: FormGroup;
  loading = false;

  employees: any[] = []
  services: any[] = []

  selectedEmployees: any[] = []
  selectedServices: any[] = []

  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';
  
  constructor(
    private auth: AuthService,
    private employeeService: EmployeeService,
    private serviceService: ServiceService,
    private clientService: ClientServiceService,
    private location: Location,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.auth.loadCurrentUser();
    this.currentUser = this.auth.getCurrentUser();
    
    this.updateEmployeeForm = this.fb.group({
      email: [''],
      gender: [''],
      job: [''],
      employment_date: ['']
    });
    this.updateClientForm = this.fb.group({
      email: [''],
      gender: [''],
      number: [''],
    })
  }
    
  get notificationLink() {
    const currentPath = this.location.path();
    if (currentPath.includes('/admin')) {
      return '/admin/notifications';
    } else {
      return '/notifications';
    }
  }

  get role(): any {
    return this.currentUser.role
  }

  ngOnInit() {
    console.log(this.currentUser)
    //employee
    if (this.currentUser.role === 'employee') {
      this.updateEmployeeForm = this.fb.group({
        email: [{value: this.currentUser.email, disabled: true}],
        gender: [this.currentUser.gender],
        job: [this.currentUser.job],
        employment_date: [this.currentUser.employment_date]
      });
    
    //client
    } else if (this.currentUser.role === 'client') {
      this.employeeService.getEmployees().subscribe({
        next: (res) => this.employees = res
      })
      this.serviceService.getServices().subscribe({
        next: (res) => this.services = res
      })
      this.updateClientForm = this.fb.group({
        email: [this.currentUser.email],
        gender: [this.currentUser.gender],
        number: [this.currentUser.number],
        service: [''],
        employee: ['']
      });
      this.selectedEmployees = this.currentUser.preferences.employees
      this.selectedServices = this.currentUser.preferences.services
      console.log(this.updateClientForm.value)
    }
  }

  addService() {
    let selectedServiceId = this.updateClientForm.value.service;
    let service = this.services.find(service => service._id === selectedServiceId);
    if (!this.selectedServices.some(selectedService => selectedService._id === service._id)) {
      this.selectedServices.push(service);
    }
  }

  removeService(service: any) {
    this.selectedServices = this.selectedServices.filter(item => item !== service);
  }

  addEmployee() {
    let selectedEmployeeId = this.updateClientForm.value.employee;
    let employee = this.employees.find(employee => employee._id === selectedEmployeeId);
    if (!this.selectedEmployees.some(selectedEmployee => selectedEmployee._id === employee._id)) {
      this.selectedEmployees.push(employee);
    }
  }

  removeEmployee(employee: any) {
    this.selectedEmployees = this.selectedEmployees.filter(item => item !== employee);
  }

  onSubmitClient() {
    this.clientService.updateClient(
      this.currentUser._id,
      this.updateClientForm.value.username,
      this.updateClientForm.value.gender,
      this.updateClientForm.value.number,
      { employees:this.selectedEmployees, services: this.selectedServices }
    ).subscribe({
      next: res => {
        this.alertMessage = res.message;
        this.alertType = 'success';
        this.showAlert = true;    
        this.currentUser = this.auth.reloadUser().subscribe({
          next: res => this.auth.getCurrentUser()
        });
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
  }

  onSubmitEmployee() {
    this.employeeService.updateEmployee(
      this.currentUser._id,
      this.updateEmployeeForm.value.username,
      this.updateEmployeeForm.value.gender,
      this.updateEmployeeForm.value.job,
      this.updateEmployeeForm.value.employment_date
    ).subscribe({
      next: res => {
        this.alertMessage = res.message;
        this.alertType = 'success';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      },
      error: err => {
        this.alertMessage = err.error.message;
        this.alertType = 'error';
        this.showAlert = true;
        setTimeout(() => this.showAlert = false, 3000);
      }
    });
  }

  logout() {
    this.loading = true
    this.auth.logout().subscribe({
      next: () => {
        this.loading = false
        setTimeout(() => {
          this.router.navigateByUrl('/auth/login')
        }, 1000);
      }
    })
  }
}
