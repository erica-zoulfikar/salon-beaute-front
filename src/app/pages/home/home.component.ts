import { Component } from '@angular/core';
import { AppointmentsService } from '../../services/appointments.service';
import { CommonModule } from '@angular/common';
import { AppointmentCardComponent } from '../../components/appointment-card/appointment-card.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, AppointmentCardComponent],
  templateUrl: './home.component.html'
})
export class HomeComponent {
  history!: any[]
  pending!: any[]

  constructor(private appointmentService: AppointmentsService) {
    this.loadAppointments()
  }

  loadAppointments() {
    this.appointmentService.getAppointmentsHistory().subscribe({
      next: res => this.history = res
    })
    this.appointmentService.getAppointmentsPendingWoDate().subscribe({
      next: res => this.pending = res
    })
  }
  
  confirmAppointment(appointment_id: String) {
    this.appointmentService.confirmAppointment(appointment_id).subscribe({
      next: res => {
        this.loadAppointments()
      }
    })
  }

  cancelAppointment(appointment_id: String) {
    this.appointmentService.cancelAppointment(appointment_id).subscribe({
      next: res => {
        this.loadAppointments()
      }
    })
  }

}
