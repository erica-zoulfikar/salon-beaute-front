import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { AlertComponent } from '../../../alert/alert.component';
import { SpinnerComponent } from '../../../components/spinner/spinner.component';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule ,RouterLink, ReactiveFormsModule, AlertComponent, SpinnerComponent],
  templateUrl: './login.component.html',
  styleUrls: ['../../../../assets/vendor/css/pages/page-auth.css'],
  providers: [AuthService]
})
export class LoginComponent {
  loginForm: FormGroup;
  
  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';

  loading = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.loginForm = this.fb.group({
      email: ['zoulfikarzh@gmail.com', [Validators.required, Validators.email]],
      password: ['password', [Validators.required]]
    });
  }

  onSubmit() {
    this.loading = true;
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe({
        next: (res) => {
          this.alertMessage = res.message || "Succès";
          this.alertType = 'success';
          this.showAlert = true;
          this.loading = false;
          setTimeout(() => {
            if (res.user.role === 'manager') {
              this.router.navigate(['admin']);
            } else {
              this.router.navigate(['']);
            }
          }, 1000);
        },
        error: (err) => {
          this.alertMessage = err.message;
          this.alertType = 'error';
          this.showAlert = true;
          this.loading = false;
          setTimeout(() => this.showAlert = false, 3000);
        }
      });
  }
}
