import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { ClientServiceService } from '../../../services/client.service';
import { AlertComponent } from '../../../alert/alert.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [CommonModule, RouterLink, ReactiveFormsModule, AlertComponent],
  templateUrl: './register.component.html',
  styleUrls: ['../../../../assets/vendor/css/pages/page-auth.css'],
})
export class RegisterComponent {
  registerForm!: FormGroup;
  
  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';

  loading = false

  constructor(private clientService: ClientServiceService, private fb:FormBuilder, private router: Router) {
    this.registerForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      role: ['client'],
      gender: ['', [Validators.required]],
      number: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.loading = true;
    this.clientService.createClient(
      this.registerForm.value.email,
      this.registerForm.value.username,
      this.registerForm.value.password,
      this.registerForm.value.role,
      this.registerForm.value.gender,
      this.registerForm.value.number
      ).subscribe({
        next: (res) => {
          this.alertMessage = res.message || "Succès";
          this.alertType = 'success';
          this.showAlert = true;
          this.loading = false;
          setTimeout(() => {
            this.router.navigate(['auth/login']);
          }, 1000);
        },
        error: (err) => {
          this.alertMessage = err.message;
          this.alertType = 'error';
          this.showAlert = true;
          this.loading = false;
          setTimeout(() => this.showAlert = false, 3000);
        }
      });
  }

}
