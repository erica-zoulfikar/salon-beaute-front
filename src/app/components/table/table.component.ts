import { CommonModule } from '@angular/common';
import { Component, Input, TemplateRef } from '@angular/core';

interface TableColumn {
  key: string;
  title: string;
  template?: TemplateRef<any>;
}

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './table.component.html'
})
export class TableComponent {
  @Input() title: string = '';
  @Input() data: any[] = [];
  @Input() columns: TableColumn[] = [];

  constructor() {
    console.log(this.columns)
  }

}
