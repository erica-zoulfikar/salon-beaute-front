import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DayPilotModule } from "@daypilot/daypilot-lite-angular";
import { CalendarComponent } from './calendar.component';
import { DataService } from './data.service';
import { AppointmentsService } from '../../services/appointments.service';
import { AlertComponent } from '../../alert/alert.component';

@NgModule({
  declarations: [
    CalendarComponent // Declare the CalendarComponent
  ],
  imports: [
    CommonModule,
    DayPilotModule,
    AlertComponent
  ],
  providers: [
    DataService,
    AppointmentsService
  ],
  exports: [
    CalendarComponent // Export CalendarComponent so it can be used in other parts of the app
  ]
})
export class CalendarModule { }
