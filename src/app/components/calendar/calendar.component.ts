import { Component, Input, ViewChild } from '@angular/core';
import { DayPilot, DayPilotCalendarComponent, DayPilotNavigatorComponent } from "@daypilot/daypilot-lite-angular";
import { AppointmentsService } from '../../services/appointments.service';
import { Observable } from 'rxjs';
import { ServiceService } from '../../services/service.service';
import { EmployeeService } from '../../services/employee.service';
import { AlertComponent } from '../../alert/alert.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrl: './calendar.component.scss',
})
export class CalendarComponent {
  @ViewChild("calendar") calendar!: DayPilotCalendarComponent;
  @ViewChild("navigator") navigator!: DayPilotNavigatorComponent;
  @Input() loadEvents!: (from: any, to: any) => Observable<any>;
  @Input() createAppointment!: (id_service: String, id_employee: String, date: String, start: String, end: String, text: String) => Observable<any>;
  @Input() updateAppointment!: (id_appointment: String, id_service: String, id_employee: String, date: String, start: String, end: String, text: String) => Observable<any>;
  @Input() deleteAppointment!: (id_appointment: String) => Observable<any>;
  @Input() role!: String

  events: any[] = [];

  services: any[] = [];
  employees: any[] = [];

  showAlert = false;
  alertMessage = '';
  alertType: 'success' | 'error' = 'success';

  constructor(private serviceService: ServiceService, private employeeService: EmployeeService) {
    this.config.startDate = DayPilot.Date.today();
    console.log(this.role)
  }

  ngOnInit(): void {
    this.serviceService.getServices().subscribe({
      next: res => this.services = res,
      error: err => console.error(err) 
    })
    this.employeeService.getEmployees().subscribe({
      next: res => this.employees = res,
      error: err => console.error(err) 
    })
  }

  ngAfterViewInit(): void {
    const from = this.calendar.control.visibleStart();
    const to = this.calendar.control.visibleEnd();
    this.loadEvents(from, to).subscribe(result => {
      this.events = result;
    })
  }

  get date(): DayPilot.Date {
    return this.config.startDate as DayPilot.Date;
  }

  set date(value: DayPilot.Date) {
    this.config.startDate = value;
  }

  navigatorConfig: DayPilot.NavigatorConfig = {
    showMonths: 3,
    skipMonths: 3,
    selectMode: "Week",
    cellWidth: 30,
    cellHeight: 30,
    dayHeaderHeight: 30,
    titleHeight: 30
  };

  config: DayPilot.CalendarConfig = {
    locale: "fr-fr",
    viewType: "Week",
    headerDateFormat: "d MMMM",
    businessBeginsHour: 7,
    businessEndsHour: 18,
    // dayBeginsHour: 7,
    // dayEndsHour: 19,
    timeRangeSelectedHandling:"Enabled",
    onTimeRangeSelected: async (args) => {
      var form = [
        {name: "Service", id: "service", options: this.services.map(service => { return { id: service._id, name: service.label}})},
        {name: "Employé", id: "employee", options: this.employees.map(employee => { return { id: employee._id, name: employee.username}})}
      ];

      await DayPilot.Modal.form(form).then((modal) => {
        if (modal.canceled) { return; }
        const service = this.services.find(service => service._id === modal.result.service);
        const employee = this.employees.find(employee => employee._id === modal.result.employee);
        this.createAppointment(
          modal.result.service,
          modal.result.employee,
          args.start.toString(),
          args.start.toString(),
          args.start.addMinutes(service.duration).toString(),
          `${service.label} ${employee.username} (${args.start.getHours()}H${args.start.getMinutes() > 0? args.start.getMinutes() : '00'} - ${args.end.getHours()}H${args.end.getMinutes() > 0? args.end.getMinutes() : '00'})`
        ).subscribe({
          next: res => {
            dp.events.add({
              start: args.start,
              end: args.start.addMinutes(service.duration),
              id: res._id | res.id,
              text: `${service.label} ${employee.username} (${args.start.getHours()}H${args.start.getMinutes() > 0? args.start.getMinutes() : '00'} - ${args.end.getHours()}H${args.end.getMinutes() > 0 ? args.end.getMinutes() : '00'})`
            });
          },
          error: err => {
            this.alertMessage = err.error.error;
            this.alertType = 'error';
            this.showAlert = true;
            setTimeout(() => {
              this.showAlert = false
            }, 2000);
          }
        })
        const dp = args.control;
        dp.clearSelection();
      });
    },
    eventMoveHandling: "Update",
    onEventMoved: async (args) => {
      const text = args.e.data.text.split(' ')
      const newStart = args.e.data.start
      const newEnd = args.e.data.end
      this.updateAppointment(
        args.e.data._id,
        args.e.data.id_service,
        args.e.data.id_employee,
        args.e.data.start,
        args.e.data.start,
        args.e.data.end,
        `${text[0]} ${text[1]} (${newStart.getHours()}H${newStart.getMinutes() > 0? newStart.getMinutes() : '00'} - ${newEnd.getHours()}H${newEnd.getMinutes() > 0? newEnd.getMinutes() : '00'})`
      ).subscribe({
        error: err => {
          this.alertMessage = err.error.error;
          this.alertType = 'error';
          this.showAlert = true;
          setTimeout(() => {
            this.showAlert = false
          }, 2000);
        }
      })
    },
    eventResizeHandling: "Disabled",
    onEventResized: (args) => {
      console.log("Event resized: " + args.e.text());
    },
    eventClickHandling: this.role === 'client' ? "ContextMenu": "Disabled",
    contextMenu: new DayPilot.Menu({
      items: [
        { text: "Supprimer", onClick: (args) => {
          const dp = args.source.calendar; 
          dp.events.remove(args.source);
          this.deleteAppointment(args.source.data._id? String(args.source.data._id) : String(args.source.data.id)).subscribe()
        }}
      ]
    }),
  };

  viewChange(): void {
    var from = this.calendar.control.visibleStart();
    var to = this.calendar.control.visibleEnd();
    this.loadEvents(from, to).subscribe(result => {
      this.events = result;
    })
  }

  navigatePrevious(event: MouseEvent): void {
    event.preventDefault();
    this.config.startDate = (this.config.startDate as DayPilot.Date).addDays(-7);
  }

  navigateNext(event: MouseEvent): void {
    event.preventDefault();
    this.config.startDate = (this.config.startDate as DayPilot.Date).addDays(7);
  }

  navigateToday(event: MouseEvent): void {
    event.preventDefault();
    this.config.startDate = DayPilot.Date.today();
  }
}
