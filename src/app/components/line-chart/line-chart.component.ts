import { Component, Input, SimpleChanges } from '@angular/core';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-line-chart',
  standalone: true,
  imports: [],
  templateUrl: './line-chart.component.html',
  styleUrl: './line-chart.component.scss'
})
export class LineChartComponent {
  @Input() labels!: any;
  @Input() data!: any;
  @Input() title!: any;

  public chart: any;

  ngOnInit(): void {
    this.initChart();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initChart();
  }

  initChart(): void {
    if (this.data && this.labels && this.title) {
      this.createChart();
    }
  }

  createChart() {
    if (this.chart) {
      this.chart.destroy();
    }

    this.chart = new Chart("lineChart", {
      type: 'line', //this denotes tha type of chart

      data: {// values on X-Axis
        labels: this.labels, 
        datasets: [{
          label: this.data.label,
          backgroundColor: "#696cff",
          data: this.data.data,
        }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
      }
      
    });
  }
}
