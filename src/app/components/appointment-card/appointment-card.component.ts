import { Component, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-appointment-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './appointment-card.component.html',
  styleUrl: './appointment-card.component.scss'
})
export class AppointmentCardComponent {
  @Input() appointment!: any
  @Input() confirm!: (appointment_id: String) => void
  @Input() cancel!: (appointment_id: String) => void

  currentUser!: any

  constructor(private auth:AuthService) { 
    this.auth.loadCurrentUser()
    this.currentUser = this.auth.getCurrentUser()
  }

  formatDate(dateString: string) {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear();
    return `${year}-${month}-${day}`;
  }

  formatTime(dateString: string) {
    const date = new Date(dateString);
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    return `${hours}H${minutes}`;
  }

  getClass() {
    if(this.appointment.status === 'confirmed') {
      return 'card bg-primary text-white mb-3'
    } else {
      return 'card bg-danger text-white mb-3'
    }
  }
}
