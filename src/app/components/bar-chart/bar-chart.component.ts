import { Component, Input, SimpleChanges } from '@angular/core';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-bar-chart',
  standalone: true,
  imports: [],
  templateUrl: './bar-chart.component.html',
  styleUrl: './bar-chart.component.scss'
})
export class BarChartComponent {
  @Input() labels!: any;
  @Input() data!: any;
  @Input() title!: any;
  @Input() loading = false;

  public chart: any;

  ngOnInit(): void {
    this.initChart();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initChart();
  }

  initChart(): void {
    this.loading = true
    if (this.data && this.labels && this.title) {
      this.loading = false
      this.createChart();
    }
  }

  createChart() {
    Chart.defaults.font.family = "Helvetica"
    if (this.chart) {
      this.chart.destroy();
    }

    this.chart = new Chart("MyChart",
      {
        type: 'bar',
        data: {
          labels: this.labels,
          datasets: [{
            label: this.data.label,
            backgroundColor: "#696cff",
            data: this.data.data, 
            barThickness: 20,
            barPercentage: 0.8, 
            categoryPercentage: 0.8, 
            borderRadius: 8,
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            x: {
              border:{
                display: false
              },
              grid: {
                color: "rgba(240, 240, 240, 240)",
                drawTicks: false,
                lineWidth: 2
              }
            },
            y: {
              border:{
                display: false
              },
              grid: {
                color: "rgba(240, 240, 240, 240)",
                drawTicks: false,
                lineWidth: 2
              }
            }
          },
          plugins: {
            title: {
              display: true,
              text: this.title
            },
            legend: {
              labels: {
                // This more specific font property overrides the global property
                font: {
                  family: 'boxicons',
                  size: 14
                }
              }
            },
            tooltip: {
              enabled: true, // Enable tooltips
              mode: 'index',
              intersect: false,
              backgroundColor: 'rgba(0, 0, 0, 0.7)', // Custom background color
              titleFont: {
                size: 16,
                weight: 'bold',
              },
              bodyFont: {
                size: 14,
              },
              footerFont: {
                size: 12,
              },
            }
          },
          animation: {
            duration: 1000, // General animation time
            easing: 'easeOutBounce', // An easing function
          },
        }
      });
  }
}
