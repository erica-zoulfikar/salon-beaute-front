import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';

@Component({
  selector: 'app-w-sidebar',
  standalone: true,
  imports: [RouterOutlet, SidebarComponent],
  templateUrl: './w-sidebar.component.html'
})
export class WSidebarComponent {

}
