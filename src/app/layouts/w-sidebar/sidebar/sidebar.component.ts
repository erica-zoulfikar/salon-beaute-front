import { Component } from '@angular/core';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive],
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent {

  constructor(private auth: AuthService, private router: Router) {
    this.auth.verifyToken().subscribe({
      error: (err) => this.router.navigateByUrl('auth/login')
    })
    this.auth.loadCurrentUser()
  }

  get isEmployee(): boolean {
    return this.auth.getRole() === "manager"
  }

  get userName(): any {
    return this.auth.getUserName()
  }
}
