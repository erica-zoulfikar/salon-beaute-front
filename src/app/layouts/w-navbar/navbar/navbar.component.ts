import { Component } from '@angular/core';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { CommonModule } from '@angular/common';
import { error } from 'console';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, CommonModule],
  templateUrl: './navbar.component.html'
})
export class NavbarComponent {
  currentUser!: any
  
  constructor(private auth: AuthService, private router: Router) {
    this.auth.verifyToken().subscribe({
      error: (err) => this.router.navigateByUrl('auth/login')
    })
    this.currentUser = auth.reloadUser()
    this.auth.loadCurrentUser()
  }

  get isEmployee(): boolean {
    return this.auth.getRole() === "manager"
  }

  get userName(): any {
    return this.auth.getUserName()
  }

  logout() {
    this.auth.logout().subscribe({
      next: () => {
        setTimeout(() => {
          this.router.navigateByUrl('/auth/login')
        }, 1000);
      }
    })
  }
}
