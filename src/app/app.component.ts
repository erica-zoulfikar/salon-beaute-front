import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [RouterOutlet, RouterLink, RouterLinkActive],
})
export class AppComponent implements OnInit {
  static isBrowser = new BehaviorSubject<boolean>(false);

  constructor(@Inject(PLATFORM_ID) private platformId: any, private router: Router) {}

  ngOnInit(): void {
    const isBrowser = isPlatformBrowser(this.platformId);
    AppComponent.isBrowser.next(isBrowser);
    if (!isBrowser) {
      this.router.navigate(['/loading']);
    }
  }

  title = 'salon-beaute-front';
}
