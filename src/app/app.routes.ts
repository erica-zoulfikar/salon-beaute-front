import { Routes } from '@angular/router';
import { BlankComponent } from './layouts/blank/blank.component';
import { WSidebarComponent } from './layouts/w-sidebar/w-sidebar.component';
import { WNavbarComponent } from './layouts/w-navbar/w-navbar.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { roleGuard } from './guards/auth.guard';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { Page404Component } from './pages/page404/page404.component';
import { Page500Component } from './pages/page500/page500.component';
import { EmployeeSettingsComponent } from './pages/settings/employee-settings/employee-settings.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ServiceSettingsComponent } from './pages/settings/service-settings/service-settings.component';
import { LoadingPageComponent } from './pages/loading-page/loading-page.component';
import { AppointmentsComponent } from './pages/appointments-history/appointments.component';
import { ReservationStatComponent } from './pages/stats/reservation-stat/reservation-stat.component';
import { ChiffredaffaireStatComponent } from './pages/stats/chiffredaffaire-stat/chiffredaffaire-stat.component';
import { ProfitStatComponent } from './pages/stats/profit-stat/profit-stat.component';
import { AverageWorkHourStatComponent } from './pages/stats/average-work-hour-stat/average-work-hour-stat.component';

export const routes: Routes = [
    {
        path: '',
        component: WNavbarComponent,
        children: [
            { path: '', component: HomeComponent },
            { path: 'loading', component: LoadingPageComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'notifications', component: NotificationsComponent },
            { path: 'appointments', component: AppointmentsComponent },
            { path: 'users', component: UserListComponent },
            { path: 'users/:id', component: UserInfoComponent },
        ]
    },
    {
        path: 'auth',
        component: BlankComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
        ]
    },
    {
        path: 'admin',
        component: WSidebarComponent,
        canActivate: [roleGuard],
        data: {role: 'manager'},
        children: [
            { path: '', component: DashboardComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'notifications', component: NotificationsComponent },
            { path: 'settings/employee', component: EmployeeSettingsComponent },
            { path: 'settings/service', component: ServiceSettingsComponent },
            { path: 'stats/reservations', component: ReservationStatComponent },
            { path: 'stats/chiffredaffaire', component: ChiffredaffaireStatComponent },
            { path: 'stats/profit', component: ProfitStatComponent },
            { path: 'stats/heure-travail', component: AverageWorkHourStatComponent },
            { path: 'users', component: UserListComponent },
            { path: 'users/:id', component: UserInfoComponent },
        ]
    },
    {
        path: '**',
        component: BlankComponent,
        children: [
            { path: '', component: Page404Component },
            { path: 'error', component: Page500Component }
        ]
    }
];
