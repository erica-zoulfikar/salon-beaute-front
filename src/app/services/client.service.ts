import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

interface Client {
  _id?: string;
  gender?: string;
  number?: string;
  preferences?: {
    employees: [],
    services: []
  },
  delete_flag?: boolean;
  id_user?: {
    _id?: string;
    email?: string;
    role?: string;
    password?: string;
    delete_flag?: boolean;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ClientServiceService {
  private apiUrl = `${environment.apiUrl}`

  constructor(private http: HttpClient) { }

  createClient(email: string, username: String, password: string, role: string, gender: string, number: string): Observable<any> {
    const body = { email, username, password , role, gender, number};
    return this.http.post<any>(`${environment.apiUrl}/api/clients`, body).pipe(
      tap(response => {
        console.log(response)
      })
    );
  }

  updateClient(id: string, username: String, gender: string, number: string, preferences: any): Observable<any> {
    const body = { gender, username, number, preferences };
    return this.http.put<any>(`${this.apiUrl}/api/clients/${id}`, body);
  }
}