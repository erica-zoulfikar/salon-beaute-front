import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { AuthService } from './auth.service';

export interface Appointments {
  id_service: String;
  id_employee: String; 
  id_client: String;
  date: String;
  status: String;
  start: Number;
  end: Number;
  delete_flag: boolean;
  text: String;
}

@Injectable({
  providedIn: 'root'
})
export class AppointmentsService {
  private apiUrl = `${environment.apiUrl}`
  currentUser: any = {};

  constructor(private http: HttpClient, private auth: AuthService) {
    this.auth.loadCurrentUser();
    this.currentUser = this.auth.getCurrentUser();
  }

  getAppointmentsPendingWoDate(): Observable<Appointments[]> {
    return this.http.get<Appointments[]>(`${this.apiUrl}/api/appointments/pending/${this.currentUser._id}`);
  }

  getAppointmentsPending(startDate: any, endDate: any): Observable<Appointments[]> {
    return this.http.get<Appointments[]>(`${this.apiUrl}/api/appointments/pending/${this.currentUser._id}?startDate=${startDate}&endDate=${endDate}`);
  }

  getAppointmentsHistory(): Observable<Appointments[]> {
    return this.http.get<Appointments[]>(`${this.apiUrl}/api/appointments/history/${this.currentUser._id}`);
  }

  createAppointment(id_service: String, id_employee: String, date: String, start: String, end: String, text: String){
    const body = { id_service, id_employee, date, start, end, text }
    if(this.currentUser.role === 'client') {
      return this.http.post<any>(`${this.apiUrl}/api/appointments/create/${this.currentUser._id}`, body);
    }
    return this.http.get<any>(`${this.apiUrl}/`);
  }

  updateAppointment(id_appointment: String, id_service: String, id_employee: String, date: String, start: String, end: String, text: String){
    const body = { id_service, id_employee, date, start, end, text }
    if(this.currentUser.role === 'client') {
    return this.http.put<any>(`${this.apiUrl}/api/appointments/update/${id_appointment}`, body);
    }
    return this.http.get<any>(`${this.apiUrl}/`);
  }

  deleteAppointment(id_appointment: String){
    return this.http.delete<any>(`${this.apiUrl}/api/appointments/${id_appointment}`);
  }

  confirmAppointment(id_appointment: String) {
    return this.http.put<any>(`${this.apiUrl}/api/appointments/${id_appointment}/confirm`, {})
  }

  cancelAppointment(id_appointment: String) {
    return this.http.put<any>(`${this.apiUrl}/api/appointments/${id_appointment}/cancel`, {})
  }

}