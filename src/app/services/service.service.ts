import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, tap, map } from 'rxjs';

interface Service {
  _id?: string;
  label?: string;
  duration?: number;
  price?: number;
  commission?: number;
  description?: string;
  image?: string;
  delete_flag?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private apiUrl = `${environment.apiUrl}`

  constructor(private http: HttpClient) { }

  formatDate(dateString: string) {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear();
    return `${year}-${month}-${day}`;
  }

  getServices(): Observable<Service[]> {
    return this.http.get<Service[]>(`${this.apiUrl}/api/services`)
  }

  getServiceById(id: string): Observable<Service> {
    return this.http.get<Service>(`${this.apiUrl}/api/services/${id}`);
  }
  
  createService(label: string, duration: number, price: number, commission: number, description: string, image: string): Observable<any> {
    const body = { label, duration , price, commission, description, image};
    return this.http.post<any>(`${environment.apiUrl}/api/services`, body);
  }
  
  updateService(id: string, label: string, duration: number, price: number, commission: number, description: string, image: string): Observable<any> {
    const body = { label, duration , price, commission, description, image};
    return this.http.put<any>(`${environment.apiUrl}/api/services/${id}`, body);
  }

  deleteService(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/api/services/${id}`);
  }

}
