import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, tap, map } from 'rxjs';

interface Employee {
  _id?: string;
  gender?: string;
  job?: string;
  employment_date?: string;
  delete_flag?: boolean;
  id_user?: {
    _id?: string;
    email?: string;
    role?: string;
    password?: string;
    delete_flag?: boolean;
  }
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private apiUrl = `${environment.apiUrl}`

  constructor(private http: HttpClient) { }

  formatDate(dateString: string) {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear();
    return `${year}-${month}-${day}`;
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.apiUrl}/api/employees`).pipe(
      map(data => data.map(user => ({
        ...user.id_user,
        ...user,
        id_user: undefined, // Assuming you want to exclude the nested id_user object
        employment_date: user.employment_date ? this.formatDate(user.employment_date) : undefined
      })))
    );
  }

  getEmployeeById(id: string): Observable<Employee> {
    return this.http.get<Employee>(`${this.apiUrl}/api/employees/${id}`).pipe(
      map(user => ({
          ...user.id_user,
          ...user,
          id_user: undefined,
          employment_date: user.employment_date ? this.formatDate(user.employment_date) : undefined
        })
      )
    );
  }
  
  createEmployee(email: string, username: String, password: string, role: string, gender: string, job: string, employment_date: Date): Observable<any> {
    const body = { email, username, password , role, gender, job, employment_date};
    return this.http.post<any>(`${environment.apiUrl}/api/employees`, body).pipe(
      tap(response => {
        console.log(response)
      })
    );
  }
  
  updateEmployee(userId: string, username: String, gender: string, job: string, employment_date: Date): Observable<any> {
    const body = { gender, username, job, employment_date};
    return this.http.put<any>(`${environment.apiUrl}/api/employees/${userId}`, body).pipe(
      tap(response => {
        console.log(response)
      })
    );
  }

  deleteEmployee(userId: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/api/employees/${userId}`).pipe(
      tap(response => {
        console.log(response)
      })
    );
  }

  
}
