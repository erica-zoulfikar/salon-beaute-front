import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

export interface Notification {
  _id: string;
  id_user : any;
  date: string;
  message: string;
  status: string;
}


@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private apiUrl = `${environment.apiUrl}`
  currentUser: any = {};

  constructor(private http: HttpClient, private auth: AuthService) {
    this.auth.loadCurrentUser();
    this.currentUser = this.auth.getCurrentUser();
  }

  getUpcomingUnreadNotifications(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/notifications/upcoming-unread/${this.currentUser._id}`);
  }

  setNotificationRead(notification_id: String): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/notifications/read/${notification_id}`, {});
  }
}