import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of, tap } from 'rxjs';
import { environment } from '../../environments/environment';
import { LocalStorageService } from './local-storage.service';
import { Router } from '@angular/router';

interface User {
  _id: string;
  email: string;
  role: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private currentUser: User | null = null;

  constructor(private http: HttpClient, private storage: LocalStorageService, private router: Router) { }

  login(email: string, password: string): Observable<any> {
    const body = { email, password };
    return this.http.post<any>(`${environment.apiUrl}/api/auth/login`, body).pipe(
      tap(response => {
        if (response.user && response.token) {
          this.currentUser = response.user;
          this.storage.setItem('access_token', response.token);
          this.storage.setItem('current_user', JSON.stringify(response.user));
        }
      })
    );
  }

  logout(): Observable<any> {
    this.currentUser = null;
    localStorage.removeItem('access_token');
    this.storage.removeItem('current_user');
    return of(null);
  }

  getRole(): string | null {
    return this.currentUser?.role ?? null;
  }

  getUserName(): string | null {
    return this.currentUser?.email ?? null;
  }

  getCurrentUser() : any {
    return this.currentUser;
  }

  verifyToken(): Observable<boolean> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/verifyToken`)
  }

  reloadUser(): Observable<boolean> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reloadUser`).pipe(
      tap((res) =>{
        console.log(res)
        this.storage.setItem('current_user', JSON.stringify(res));
        this.currentUser = res.user
      }),
      catchError(error => {
        if (error.status === 500) {
          this.router.navigateByUrl('error');
          return of(false);
        }
        this.router.navigateByUrl('auth/login');
        return of(false);
      })
    );
  }
  
  loadCurrentUser(): void {
    const userJson = this.storage.getItem('current_user');
    if(userJson){
      this.currentUser = JSON.parse(userJson)
    }else{
      this.router.navigateByUrl('auth/login');
    }
  }
}
