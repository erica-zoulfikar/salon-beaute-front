import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval, map, tap } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatsService {
  private apiUrl = `${environment.apiUrl}`

  constructor(private http: HttpClient) {
  }

  formatDate(dateString: any, type: String) {
    if(type === 'day'){
      const date = new Date(dateString);
      const day = date.getDate().toString().padStart(2, '0');
      const month = (date.getMonth() + 1).toString().padStart(2, '0'); 
      return `${day}/${month}`;
    }else {
      const d = new Date(dateString);
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      const month = months[d.getMonth()];
      return `${month}`;
    }
  }

  getChiffreDaffaireJour(dateDebut: any, dateFin: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/statistics/chiffredaffaire/jour?dateDebut=${dateDebut}&dateFin=${dateFin}`).pipe(
      map(res => {
        const labels = Object.keys(res.data).map(date => this.formatDate(date, 'day'));
        const dataPoints = Object.values(res.data);
        const data = {
          label: 'Chiffre d\'affaire',
          data: dataPoints
        };
        const title = res.label;
  
        return { labels, data, title };
      }),
    );
  }

  getChiffreDaffaireMois(dateDebut: any, dateFin: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/statistics/chiffredaffaire/mois?dateDebut=${dateDebut}&dateFin=${dateFin}`).pipe(
      map(res => {
        const labels = Object.keys(res.data).map(date => this.formatDate(date, 'month'));
        const dataPoints = Object.values(res.data);
        const data = {
          label: 'Chiffre d\'affaire',
          data: dataPoints
        };
        const title = res.label;
  
        return { labels, data, title };
      }),
    );
  }

  getReservationsPerDay(dateDebut: any, dateFin: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/statistics/reservationsPerDay?dateDebut=${dateDebut}&dateFin=${dateFin}`).pipe(
      map(res => {
        const labels = res.map((item: any) => this.formatDate(item.label, 'day'));
        const data = {
          label: 'Reservations',
          data: res.map((item: any) => item.data)
        };
        const title = "Réservations par jour"
        return { labels, data, title };
      }),
    );
  }

  getReservationsPerMonth(dateDebut: any, dateFin: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/statistics/reservationsPerMonth?dateDebut=${dateDebut}&dateFin=${dateFin}`).pipe(
      map(res => {
        const labels = res.map((item: any) => this.formatDate(item.label, 'month'));
        const data = {
          label: 'Reservations',
          data: res.map((item: any) => item.data)
        };
        const title = "Réservations par mois"
        return { labels, data, title };
      }),
    );
  }

  getAverageWorkTimeEmp(date: any, interval: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/statistics/averageWorkTimeEmp?date=${date}&interval=${interval}`).pipe(
      tap(res => console.log(res)),
    );
  }

  getProfitPerMonth(dateDebut: any, dateFin: any, salaire: Number, loyer: Number, achatPiece: Number, autresDepenses: Number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/statistics/profitpermonth?dateDebut=${dateDebut}&dateFin=${dateFin}&salaire=${salaire}&loyer=${loyer}&achatPiece=${achatPiece}&autresDepenses=${autresDepenses}`).pipe(
      map(res => {
        const labels = res.map((item: any) => item.label);
        const dataPoints = res.map((item: any) => item.data);
        const data = {
          label: 'Chiffre d\'affaire',
          data: dataPoints,
        };
        const title = "Profit par mois";
        return { labels, data, title };
      }),
    );
  }
}
