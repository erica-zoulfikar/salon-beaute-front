import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-info',
  standalone: true,
  imports: [],
  templateUrl: './user-info.component.html'
})
export class UserInfoComponent {
  @Input() id!: string;

  user: any = {};

  constructor (private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getUserById(this.id).subscribe((data) => {
      this.user = data;
    });
  }
}
