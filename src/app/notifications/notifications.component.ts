import { CommonModule, Location } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { TableComponent } from '../components/table/table.component';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-notifications',
  standalone: true,
  imports: [RouterLink, TableComponent, CommonModule],
  templateUrl: './notifications.component.html',
  styleUrl: './notifications.component.scss'
})
export class NotificationsComponent {
  notifications: any[] = [{message: "Aucune nouvelles notifications", status: "read"}]

  constructor(private location: Location, private notificationService: NotificationService) {
    this.notificationService.getUpcomingUnreadNotifications().subscribe({
      next: res => res.length > 0? this.notifications = res : this.notifications,
      error: err => console.log(err),
    })
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.notifications.map(notification => {
        if (notification._id){
          this.notificationService.setNotificationRead(notification._id).subscribe();
          notification.status = "read";
        }
      })
    }, 3000)
  }

  get profileLink() {
    const currentPath = this.location.path();
    if (currentPath.includes('/admin')) {
      return '/admin/profile';
    } else {
      return '/profile';
    }
  }
}
